<!DOCTYPE html>
<html lang=fi>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <script src="https://cdn.tailwindcss.com"></script>

        <style>
            .button {
                background-color: #4CAF50;
                border: none;
                color: white;
                padding: 15px 32px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
            }
        </style>

        <title>Levykauppa-Y</title>

        <!-- Fonts -->
        <link href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    
    <body class="antialiased">
        <h1 class="font-medium leading-tight text-5xl mt-0 mb-2 text-blue-600">
            Levykauppa-Y
        </h1>

        <a href="/ostoskori">Ostoskori</a>
        &ensp;
        
        @auth
        <span class="font-bold uppercase">
            Tervetuloa "{{auth()->user()->name}}"
        </span>
        &ensp;

        <form class="inline" method="POST" action="/logout">
            @csrf
            <button type="submit">
                <i class="fa-solid fa-door-closed"></i>Logout
            </button>
        </form>

        @else

        <a href="/register">Register</a>
        &ensp;
        <a href="/login">Login</a>

        @endauth

        <br/>

        @yield('content')
    </body>

</html>
