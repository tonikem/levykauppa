@extends('index')

@section('content')
    @foreach($levyt as $levy)
        <br/><br/>
        <img src="{{$levy['data']}}" width="200" height="200"/>
        <h3><a href={{$levy['id']}}>{{$levy['otsikko']}}</a></h3>
        <p>Hinta: {{$levy['hinta']}} €</p>
    @endforeach
@endsection
