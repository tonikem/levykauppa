@extends('index')

@section('content')

<ul>
    <p id="levy-1"></p>
    <p id="levy-2"></p>
    <p id="levy-3"></p>
    <p id="levy-4"></p>
    <p id="summa"></p>
    <button onclick="tyhjenna()">Tyhjennä kori</button>
</ul>

<script>
    var hinta = 0;
    var levy1 = Number.parseInt(localStorage.getItem("levy-1"));
    var levy2 = Number.parseInt(localStorage.getItem("levy-2"));
    var levy3 = Number.parseInt(localStorage.getItem("levy-3"));
    var levy4 = Number.parseInt(localStorage.getItem("levy-4"));
    
    if (levy1) {
        var summa = levy1 * 31.95
        hinta = hinta + summa
        document.getElementById("levy-1").innerHTML = "In Flames : Foregone 2-LP " + levy1 + " kpl " + summa + "€"
    }
    if (levy2) {
        var summa = levy2 * 39.95
        hinta = hinta + summa
        document.getElementById("levy-2").innerHTML = "Vesala : Näkemiin, melankolia LP " + levy2 + " kpl " + summa + "€"
    }
    if (levy3) {
        var summa = levy3 * 29.95
        hinta = hinta + summa
        document.getElementById("levy-3").innerHTML = "Judas Priest : Defenders of the Faith LP " + levy3 + " kpl " + summa + "€"
    }
    if (levy4) {
        var summa = levy4 * 19.95
        hinta = hinta + summa
        document.getElementById("levy-4").innerHTML = "D/Troit : Soul Sound System LP " + levy4 + " kpl " + summa + "€"
    }

    if (summa > 0) {
        document.getElementById("summa").innerHTML = "Summa: " + hinta
    }

    function tyhjenna() {
        localStorage.setItem("levy-1", null);
        localStorage.setItem("levy-2", null);
        localStorage.setItem("levy-3", null);
        localStorage.setItem("levy-4", null);
        document.getElementById("levy-1").innerHTML = ""
        document.getElementById("levy-2").innerHTML = ""
        document.getElementById("levy-3").innerHTML = ""
        document.getElementById("levy-4").innerHTML = ""
    }

</script>

@endsection
