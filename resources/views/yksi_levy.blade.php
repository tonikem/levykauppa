@extends('index')

@section('content')
    <br/><br/><br/>
    <img src="{{$levy['data']}}" width="400" height="400"/>
    <h3><a href=/{{$levy['id']}}>{{$levy['otsikko']}}</a></h3>
    <p>Hinta: {{$levy['hinta']}} €</p>
    <button class="button" onclick="osta({{$levy['id']}})">Osta</button>
    <p id="korissa">Korissa: 0</p>
    <script>
        function osta(id) {
            var levyId = "levy-"+id;
            var levy = Number.parseInt(localStorage.getItem(levyId));

            if (levy) {
                localStorage.setItem(levyId, levy + 1);
            } else {
                localStorage.setItem(levyId, 1);
            }
            
            var levy = Number.parseInt(localStorage.getItem(levyId));
            document.getElementById("korissa").innerHTML = "Korissa: " + levy 

            console.log(localStorage.getItem(levyId));
        }

        var levyId = "levy-" + {{$levy['id']}};
        var levy = Number.parseInt(localStorage.getItem(levyId));
        document.getElementById("korissa").innerHTML = "Korissa: " + levy 
    </script>
@endsection
