<?php

use App\Models\Levy;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('levyt', [
        'levyt' => Levy::all()
    ]);
});

Route::get('/{id}', function ($id) {
    return view('yksi_levy', [
        'levy' => Levy::find($id)
    ]);
})->where('id', '[0-9]+');

Route::get('/ostoskori', function() {
    return view("ostoskori");
});

Route::get('/register', [UserController::class, 'create']);

Route::post('/users', [UserController::class, 'store']);

Route::post('/logout', [UserController::class, 'logout']);

Route::get('/login', [UserController::class, 'login']);

Route::post('/users/authenticate', [UserController::class, 'authenticate']);
